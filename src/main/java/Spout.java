import org.apache.log4j.Logger;
import org.apache.storm.shade.com.google.common.io.Resources;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

import java.io.*;
import java.util.*;

public class Spout extends BaseRichSpout {

    private static final Logger logger = Logger.getLogger(Spout.class);

    private SpoutOutputCollector collector;
    private BufferedReader gtcFileBR;
    private BufferedReader edgesFileBR;

    private boolean streamOver;

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {

        this.collector = collector;

        try {
            gtcFileBR = new BufferedReader(new FileReader(new File(Resources.getResource("amazonGTC.txt").getFile())));
            edgesFileBR = new BufferedReader(new FileReader(new File(Resources.getResource("amazon.txt").getFile())));
        }
        catch (FileNotFoundException e){
            logger.error(e);
        }
    }

    @Override
    public void nextTuple() {

        String line;

        try {
            if ((line = gtcFileBR.readLine()) != null || (line = edgesFileBR.readLine()) != null) {
                collector.emit(new Values(line));
            }
            else if (!streamOver) {
                collector.emit(new Values("stop"));
                streamOver = true;
            }
        }
        catch (IOException e) {
            logger.error(e);
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("spoutOutputField"));
    }
}