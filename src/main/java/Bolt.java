import org.apache.log4j.Logger;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

import java.util.*;
import java.util.Map.Entry;

import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;

public class Bolt extends BaseRichBolt {

    private static final Logger logger = Logger.getLogger(Bolt.class);

    private final int NUM_OF_SEEDS = 3;

    private List<Community> communities;
    private Sketch cSketch;
    private Sketch dSketch;
    private int edgesProcessed;
    private int total;
    private int count;
    private Random rand;

    @Override
    public void prepare(Map conf, TopologyContext context, OutputCollector collector) {

        communities = new ArrayList<>();
        cSketch = new Sketch(0.00001, 0.99);
        dSketch = new Sketch(0.00001, 0.99);
        edgesProcessed = 0;
        total = 0;
        count = 0;
        rand = new Random();
    }

    private double getF1Score(Set<String> found, String[] comm) {

        HashSet<String> gtc = new HashSet<>(Arrays.asList(comm));
        SetView<String> common = Sets.intersection(found, gtc);

        double precision = (double) common.size() / found.size();
        double recall = (double) common.size() / gtc.size();

        if (precision == 0 && recall == 0)
            return 0;
        else
            return 2 * (precision * recall) / (precision + recall);
    }

    private void dropTail(Community community) {

        int bestSize;
        List<Entry<String, Double>> sortedCommunity = community.getSortedCommunity();

        // calculate mean distance
        Double previous = null;
        double totalDifference = 0D;
        int totalDifferenceCount = 0;
        for (int i = NUM_OF_SEEDS; i < sortedCommunity.size(); i++) {
            Entry<String, Double> entry = sortedCommunity.get(i);
            if (previous != null && !community.isSeed(entry.getKey())) {
                totalDifference += previous - entry.getValue();
                totalDifferenceCount++;
            }
            previous = entry.getValue();
        }
        double meanDifference = totalDifference / totalDifferenceCount;

        List<Entry<String, Double>> reverseSortedCommunity = community.getSortedCommunity();
        Collections.reverse(reverseSortedCommunity);
        previous = null;
        int tailSize = 0;
        for (Entry<String, Double> entry : reverseSortedCommunity) {
            tailSize++;
            if (previous != null) {
                double difference = entry.getValue() - previous;
                if (difference > meanDifference) {
                    break;
                }
            }
            previous = entry.getValue();
        }

        bestSize = community.size() - tailSize;
        try {
            total += getF1Score(community.getPrunedCommunity(bestSize).keySet(), community.getGroundTruth());
            count += 1;
        } catch (Exception e) {
            logger.error(e);
        }
    }

    @Override
    public void execute(Tuple tuple) {

        String[] nodes = tuple.getString(0).split(" ");

        if (nodes.length == 1) { // dropTail and print F1-SCORE

            communities.forEach(community -> dropTail(community));

            logger.info("\n\nF1-SCORE = " + (double) total / count + "\n\n");
        }
        else if (nodes.length > 2) { // gtc

            Set<String> seeds = new HashSet<>();

            while (seeds.size() < NUM_OF_SEEDS) {
                seeds.add(nodes[rand.nextInt(nodes.length)]);
            }

            communities.add(new Community(seeds, nodes));
        }
        else if (!nodes[0].equals(nodes[1])) { // edge

            edgesProcessed++;
            dSketch.add(nodes[0], 1);
            dSketch.add(nodes[1], 1);

            for (int i = 0; i < communities.size(); i++) {

                Community comm = communities.get(i);

                // if adjacent node is a seed, add 1
                if (comm.isSeed(nodes[0])) {
                    cSketch.add(i + ":" + nodes[1], 1);
                }
                // else if adjacent node is a member add estimate of participation /
                // estimate of degree
                else if (comm.contains(nodes[0])) {
                    cSketch.add(i + ":" + nodes[1], comm.get(nodes[0]));
                }

                // if adjacent node is a seed, add 1
                if (comm.isSeed(nodes[1])) {
                    cSketch.add(i + ":" + nodes[0], 1);
                }
                // else if adjacent node is a member add estimate of participation /
                // estimate of degree
                else if (comm.contains(nodes[1])) {
                    cSketch.add(i + ":" + nodes[0], comm.get(nodes[1]));
                }

                // if adjacent node is a member add node to community
                if (comm.contains(nodes[0])) {
                    comm.put(nodes[1], 1 - ((dSketch.estimateCount(nodes[1]) -
                            cSketch.estimateCount(i + ":" + nodes[1])) / dSketch.estimateCount(nodes[1])));
                }

                // if adjacent node is a member add node to community
                if (comm.contains(nodes[1])) {
                    comm.put(nodes[0], 1 - ((dSketch.estimateCount(nodes[0]) -
                            cSketch.estimateCount(i + ":" + nodes[0])) / dSketch.estimateCount(nodes[0])));
                }
            }

            if (edgesProcessed % 10000 == 0) {
                communities.parallelStream().forEach(community ->
                        community.pruneCommunity(100));
            }
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("boltOutputField"));
    }
}