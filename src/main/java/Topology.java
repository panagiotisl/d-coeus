import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;

public class Topology {

    public static void main(String[] args) {

        TopologyBuilder builder = new TopologyBuilder();

        builder.setSpout("spoutID", new Spout(), 1);
        builder.setBolt("boltID", new Bolt(), 1).shuffleGrouping("spoutID");

        Config conf = new Config();
        conf.setDebug(true);

        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("Topology", conf, builder.createTopology());
    }
}